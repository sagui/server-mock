const express = require('express')
const https = require('https')
const fs = require('fs')
const cors = require('cors')
const app = express()

app.use(cors())
app.use('/api', (req, res) => res.send('Hello API'))
app.use('/', (req, res) => res.send('Hello'))

const credentials = {
  key: fs.readFileSync('server-key.pem'),
  cert: fs.readFileSync('server.pem')
}

https.createServer(credentials, app).listen(8080, () => console.log('Running...'));

/* res.setHeader('Access-Control-Allow-Origin', 'https://localhost:*')
res.setHeader('Access-Control-Request-Method', '*')
res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET')
res.setHeader('Access-Control-Allow-Headers', '*')

if (req.method === 'OPTIONS' || req.method === 'GET') {
  res.writeHead(200)
  res.end()
  return
} */
