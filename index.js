const express = require('express')
const app = express()
const server = require('http').createServer(app)
const io = require('socket.io')(server)

const APIResquest = require('./API')

app.use('/', (req, res) => res.send('Hello'))

io.on('connection', socket => {
  // console.log('conected', socket.id)

  socket.on('guias', async () => {
    await APIResquest({
      uri: 'guias',
      method: 'GET',
    })
      .then(data => socket.broadcast.emit('success', data))
      .catch(err => socket.emit('error', { connection: 'fail' }));
  })

  socket.on('disconnect', () => console.log('Client disconnected'))
})

server.listen(4001, () => console.log('running...'))
