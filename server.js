/*
  GET:
  http://localhost:8080/guias

  DELETE:
  http://localhost:8080/guias/3

  GET WITH PARAMS:
  http://localhost:8080/guias?first_name=Sebastian
  http://localhost:8080/guias?q=Smith
 */

const jsonServer = require('json-server')
const server = jsonServer.create()
const router = jsonServer.router('./db.json')
const middlewares = jsonServer.defaults()
const port = process.env.PORT || 8080

server.use(middlewares)
server.use(router)
server.listen(port)


/**
 * SERVER SOCKET.IO
 */
/* const express = require('express')
const app = express()
const cors = require('cors')
const server = require('http').createServer(app)
const io = require('socket.io')(server)
const port = process.env.PORT || 8080

app.use(cors())

app.get('/guias', (req, res) => {
  res.send('hello')
});

io.on('connection', client => {
  console.log('Client connected...')
  client.on('guias', () => {
    client.emit('success', { connected: true })
  });
});

server.listen(port) */
